# The Laravel Development Stack And You #

The docker compose file by default has the following defined containers:

* The latest minor version of php 8, the php-fpm and composer (app-runtime)
* Nginx version 1.21.0 (app-web-nginx)
* Mysql version 8.0.16 (app-database)
* Nodejs version 14.15.5 (app-node-js)

app-runtime can be used for running artisan commands, running composer package install commands and unit tests. Nodejs can be used for installing npm packages or running webpack builds. Instructions for this listed below.

### A Quick Note On Testing ###
All unit testing should be designed to be Independent of any external dependency or system. Database calls, Kafka producers, API calls, etc. should all be mocked.
___
# Using The Development Stack #

## Launching The App Stack ##

The entire stack can be launched using the following command:
```bash
docker-compose up -d 
```

if you wish to view direct to terminal output from each container, this can be done so without the detach flag:
```bash
docker-compose up
```
Note: this is a blocking command.
___

## Terminating The App Stack ##
To stop all the containers in the stack the following command can be used:
```bash
docker-compose down
```
Note: If you are using the local Kafka instance as listen below, you may get warnings about the entanet shared network, this is normal.
___

## Running Unit Tests ##
To Run unit tests simply invoke the phpunit executable as you would locally inside the container like so:
```bash
docker-compose exec app-runtime vendor/bin/phpunit
```
___
## Running Composer Commands ##
Composer installed within the app-runtime container. The docker-compose exec command can be used to run composer like so:
```bash
docker-compose exec app-runtime composer install
```
```bash
docker-compose exec app-runtime composer install aws/aws-sdk-php
```
___
## Running Artisan Commands ##
Artisan commands can be run within the app-runtime container in exactly the same way as composer commands:
```bash
docker-compose exec app-runtime php artisan migrate:fresh
```
```bash
docker-compose exec app-runtime php artisan db:seed
```
___
## Running NPM And Webpack Commands ##
Similarly to the above docker-compose exec can also be used to run node or Webpack commands using the dedicated Nodejs container like so:
```bash
docker-compose exec app-node-js npm install
```
```bash
docker-compose exec app-node-js npm run dev
```
___
## Using Local Kafka To Test Producers And Consumers ##
A local Kafka instance is available if the testing of consumers or publishers is needed. This is a separate repository and will need to be cloned and run independently of this development stack. The ```entanet-shared``` network is used for communication between the app and the broker. Adding the hostname of ```Kafka:9092``` into the environment file broker config will be needed for the local Kafka to work.
[Local development Kafka is available here.](https://bitbucket.org/entanet_dev/entanet-shared-docker/src/master/docker-compose.yml "Local development Kafka")
___

## Getting Terminals On Containers ##
It may be useful to get interactive terminals attached to running containers for running things like watch commands for example. This can be done with the following command:
```bash
docker exec -ti <container-name> /bin/bash
```
Note: Most of the containers (app-runtime, app-web-nginx, app-database) have the bash shell available. The container app-node-js does not, an interactive shell can still be started with:
```bash
docker exec -ti <container-name> /bin/sh
```
